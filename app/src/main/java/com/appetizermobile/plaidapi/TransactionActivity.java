package com.appetizermobile.plaidapi;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.appetizermobile.plaidapi.Models.Transaction;
import com.appetizermobile.plaidapi.database.TransactionCursorWrapper;
import com.appetizermobile.plaidapi.database.TransactionDbHelper;
import com.appetizermobile.plaidapi.database.TransactionDbSchema;
import com.appetizermobile.plaidapi.database.TransactionDbSchema.TransactionTable;
import com.appetizermobile.plaidapi.retrofit.ServiceGenerator;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionActivity extends AppCompatActivity {
    private static final String TAG = "TransactionActivity";

    private static final String EXTRA_ACCOUNT_NAME = "com.appetizermobile.plaidapi.account_name";
    private static final String EXTRA_ACCESS_TOKEN = "com.appetizermobile.plaidapi.access_token";
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    private static final PlaidApi.PlaidApiService SERVICE =
            ServiceGenerator.createService(PlaidApi.PlaidApiService.class);

    private RecyclerView mTransactionsView;
    private TextView mAccountName;

    private Button mExportButton;
    private Button mGetTransactions;
    private SQLiteDatabase mSQLiteDatabase;
    private ArrayList<Transaction> mTransactions;

    public static Intent newIntent(Context packageContext,
                                   String accessToken,
                                   String accountName) {
        Intent i = new Intent(packageContext, TransactionActivity.class);
        i.putExtra(EXTRA_ACCESS_TOKEN, accessToken);
        i.putExtra(EXTRA_ACCOUNT_NAME, accountName);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);
        Log.d(TAG, "TransactionActivity started");
        Log.d(TAG, "Access Token= " + getIntent().getStringExtra(EXTRA_ACCESS_TOKEN));

        mAccountName = (TextView) findViewById(R.id.transaction_text_view);

        mTransactionsView = (RecyclerView) findViewById(R.id.transactions_recycler_view);
        mTransactionsView.setLayoutManager(new LinearLayoutManager(this));

        mExportButton = (Button) findViewById(R.id.export_button);
        mExportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(TransactionActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(TransactionActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                } else {
                    exportDb(mTransactions);
                }
            }
        });


        mGetTransactions = (Button) findViewById(R.id.get_transactions);
        mGetTransactions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTransactions();
            }
        });

        getTransactions();

    }

    private void getTransactions() {

        Call<PlaidApi.PlaidResponse> getTransactions = SERVICE.getTransactions(
                Constant.clientId,
                Constant.secret,
                getIntent().getStringExtra(EXTRA_ACCESS_TOKEN),
                ""

        );

        getTransactions.enqueue(new Callback<PlaidApi.PlaidResponse>() {
            @Override
            public void onResponse(Call<PlaidApi.PlaidResponse> call,
                                   Response<PlaidApi.PlaidResponse> response) {
                Log.d(TAG, "Response code for callForTransaction= " + response.code());

                if (response.isSuccessful()) {

                    PlaidApi.PlaidResponse decodedResponse = response.body();

                    parseTransactions(decodedResponse.transactions);
                    Log.d(TAG, "Transactions= " + decodedResponse.transactions);
                } else {
                    Log.d(TAG, "getTransactions() was unsuccessful");
                }
            }

            @Override
            public void onFailure(Call<PlaidApi.PlaidResponse> call, Throwable t) {
                Log.e(TAG, "onFailure", t);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {

                if (grantResults.length > 0 && grantResults[0]
                        == PackageManager.PERMISSION_GRANTED) {
                    exportDb(mTransactions);
                } else {
                    //Permission denied
                }
                return;
            }
        }
    }

    private void parseTransactions(List<Map<String, Object>> rawTransactionInfo) {
        ArrayList<Transaction> transactions = new ArrayList<>();

        for (int i = 0; i < rawTransactionInfo.size(); i++) {
            String transactionAccount =
                    (String) rawTransactionInfo.get(i).get("_account");

            String transactionId =
                    (String) rawTransactionInfo.get(i).get("_id");

            String transactionDate =
                    (String) rawTransactionInfo.get(i).get("date");

            Double transactionAmount =
                    (Double) rawTransactionInfo.get(i).get("amount");

            String transactionName =
                    (String) rawTransactionInfo.get(i).get("name");

            Map<String, Map<String, Object>> metaMap =
                    (Map<String, Map<String, Object>>) rawTransactionInfo.get(i).get("meta");

            Map<String, Object> locationMap = metaMap.get("location");

            String city = (String) locationMap.get("city");
            String state = (String) locationMap.get("state");
            String address = (String) locationMap.get("address");

            String location = null;

            if (city == null && address == null && state == null) {
                location = "";
            } else if (address == null && city != null && state != null) {
                location = city + ", " + state;
            } else {
                location = address + ", " + city + ", " + state;
            }

            Map<String, Double> coordinatesMap =
                    (Map<String, Double>) locationMap.get("coordinates");


            Double latitude = null;
            Double longitude = null;

            if (coordinatesMap != null) {
                latitude = coordinatesMap.get("lat");
                longitude = coordinatesMap.get("lon");
            }

            String latitudeString = null;
            String longitudeString = null;
            String coordinates = null;

            if (latitude != null || longitude != null) {
                latitudeString = Double.toString(latitude);
                longitudeString = Double.toString(longitude);
                coordinates = latitudeString + ", " + longitudeString;
            } else {
                coordinates = "";
            }


            ArrayList<String> categories =
                    (ArrayList<String>) rawTransactionInfo.get(i).get("category");

            String category = ("" + categories);

            if (category.equals("null")) {
                category = "";
            } else {
                category = category.substring(1, category.length() - 1);
            }

            Transaction transaction =
                    new Transaction(
                            transactionAccount,
                            transactionId,
                            transactionAmount,
                            transactionDate,
                            transactionName,
                            location,
                            coordinates,
                            category
                    );
            transactions.add(transaction);


            mTransactions = transactions;


        }

        Collections.sort(transactions);
        displayTransactions(transactions);

        Log.d(TAG, "# of transacions= " + transactions.size());
        Log.d(TAG, "Total transactions= " + transactions);
        Log.d(TAG, "Account Name= " + getIntent().getStringExtra(EXTRA_ACCOUNT_NAME));


    }

    private void displayTransactions(List<Transaction> transactions) {
        mAccountName.setText("Transactions for " + getIntent().getStringExtra(EXTRA_ACCOUNT_NAME));
        mTransactionsView.setAdapter(new TransactionAdapter(transactions));
    }

    private class TransactionHolder extends RecyclerView.ViewHolder {

        private TextView mTransactionName;
        private TextView mTransactionLocation;
        private TextView mTransactionAmount;
        private TextView mTransactionDate;
        private TextView mTransactionCategory;
        private TextView mTransactionCoordinates;

        public TransactionHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.transaction_info, parent, false));

            mTransactionName = (TextView) itemView.findViewById(R.id.transaction_name);
            mTransactionLocation = (TextView) itemView.findViewById(R.id.transaction_location);
            mTransactionAmount = (TextView) itemView.findViewById(R.id.transaction_amount);
            mTransactionDate = (TextView) itemView.findViewById(R.id.transaction_date);
            mTransactionCategory = (TextView) itemView.findViewById(R.id.transaction_category);
            mTransactionCoordinates =
                    (TextView) itemView.findViewById(R.id.transaction_coordinates);

        }

        public void bindTransaction(Transaction transaction) {
            mTransactionName.setText(transaction.getName());

            mTransactionLocation.setText(transaction.getLocation());

            Double amount = transaction.getAmount();

            if (amount < 0) {
                String amountIntoAccount = "+" +
                        Double.toString(amount).substring(1, Double.toString(amount).length());
                mTransactionAmount.setText(amountIntoAccount);
                mTransactionAmount.setTextColor(getResources().getColor(R.color.green));
            } else {
                String amountOutOfAccount = "-" + Double.toString(amount);
                mTransactionAmount.setText(amountOutOfAccount);
                mTransactionAmount.setTextColor(getResources().getColor(R.color.red));
            }

            Date transactionDate = transaction.getDate();

            mTransactionDate.setText(Constant.getMonth(transactionDate.getMonth())
                    + " " + transactionDate.getDate());

            String category = transaction.getCategories();

            mTransactionCategory.setText(category);

            mTransactionCoordinates.setText(transaction.getCoordinates());

            if (mTransactionCoordinates.getText().toString().equals("")) {
                mTransactionCoordinates.setVisibility(View.GONE);
            }
        }
    }

    private class TransactionAdapter extends RecyclerView.Adapter<TransactionHolder> {
        private List<Transaction> mTransactions;

        public TransactionAdapter(List<Transaction> transactions) {
            mTransactions = transactions;
        }

        @Override
        public TransactionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(TransactionActivity.this);
            return new TransactionHolder(inflater, parent);
        }

        @Override
        public void onBindViewHolder(TransactionHolder holder, int position) {
            holder.bindTransaction(mTransactions.get(position));
        }

        @Override
        public int getItemCount() {
            return mTransactions.size();
        }
    }

    private static ContentValues getContentValues(Transaction transaction) {
        ContentValues values = new ContentValues();
        values.put(TransactionTable.Cols.TRANSACTION_ACCOUNT, transaction.getAccount());
        values.put(TransactionTable.Cols.TRANSACTION_ID, transaction.getId());
        values.put(TransactionTable.Cols.TRANSACTION_AMOUNT, transaction.getAmount());
        values.put(TransactionTable.Cols.TRANSACTION_NAME, transaction.getName());

        String transactionDate = Constant.getMonth(transaction.getDate().getMonth())
                + " " + transaction.getDate().getDate();
        values.put(TransactionTable.Cols.TRANSACTION_DATE, transactionDate);

        values.put(TransactionTable.Cols.TRANSACTION_LOCATION, transaction.getLocation());
        values.put(TransactionTable.Cols.TRANSACTION_CATEGORY, transaction.getCategories());
        values.put(TransactionTable.Cols.TRANSACTION_CATEGORY_ONE, transaction.getCategoryLevel1());
        values.put(TransactionTable.Cols.TRANSACTION_CATEGORY_TWO, transaction.getCategoryLevel2());
        values.put(
                TransactionTable.Cols.TRANSACTION_CATEGORY_THREE,
                transaction.getCategoryLevel3()
        );
        values.put(TransactionTable.Cols.TRANSACTION_COORDINATES, transaction.getCoordinates());

        return values;
    }

    private TransactionCursorWrapper queryTransactions(String whereClause, String[] whereArgs) {

        Cursor cursor = mSQLiteDatabase.query(
                TransactionTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );

        return new TransactionCursorWrapper(cursor);
    }

    private class ExportDataBaseToCSVTask extends AsyncTask<String, Void, Boolean> {
        private final ProgressDialog mProgressDialog = new ProgressDialog(TransactionActivity.this);


        @Override
        protected void onPreExecute() {
            this.mProgressDialog.setMessage("Exporting database to csv...");
            this.mProgressDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            File dbFile = getDatabasePath("transactions.db");
            TransactionDbHelper dbHelper = new TransactionDbHelper(TransactionActivity.this);
            Log.d(TAG, "Database path= " + dbFile);

            File exportDir = new File(Environment.getExternalStorageDirectory(), "");

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }

            File file = new File(exportDir, "plaidTransactions.csv");

            try {
                if (file.createNewFile()) {
                    Log.d(TAG, "File is created!");
                    Log.d(TAG, "transactionsCSV.csv " + file.getAbsolutePath());
                } else {
                    Log.d(TAG, "File already exists");
                }

                CSVWriter csvWriter = new CSVWriter(new FileWriter(file));

                TransactionCursorWrapper curCSV = queryTransactions(null, null);
                csvWriter.writeNext(curCSV.getColumnNames());

                while (curCSV.moveToNext()) {
                    String arrStr[] = {
                            curCSV.getString(0),
                            curCSV.getString(1),
                            curCSV.getString(2),
                            curCSV.getString(3),
                            curCSV.getString(4),
                            curCSV.getString(5),
                            curCSV.getString(6),
                            curCSV.getString(7),
                            curCSV.getString(8),
                            curCSV.getString(9),
                            curCSV.getString(10),
                            curCSV.getString(11)
                    };

                    csvWriter.writeNext(arrStr);
                }

                csvWriter.close();
                curCSV.close();

                return true;

            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);

                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (this.mProgressDialog.isShowing()) {
                this.mProgressDialog.dismiss();
            }

            if (aBoolean) {
                Toast.makeText(
                        TransactionActivity.this,
                        "Export succeeded",
                        Toast.LENGTH_SHORT
                ).show();
            } else {
                Toast.makeText(
                        TransactionActivity.this,
                        "Export failed",
                        Toast.LENGTH_SHORT
                ).show();
            }
        }
    }

    private void exportDb(ArrayList<Transaction> transactions) {
        mSQLiteDatabase = new TransactionDbHelper(TransactionActivity.this).getWritableDatabase();
        mSQLiteDatabase.delete(TransactionTable.NAME, null, null);

        for (Transaction transaction : transactions) {
            ContentValues values = getContentValues(transaction);
            mSQLiteDatabase.insert(TransactionTable.NAME, null, values);
        }

        File database = TransactionActivity.this.getDatabasePath("transactions.db");
        if (!database.exists()) {
            Log.d(TAG, "Database does not exist");
        } else {

            new ExportDataBaseToCSVTask().execute("");
        }

    }

}
