package com.appetizermobile.plaidapi;

/**
 * Created by garrettfitzgerald on 8/10/16.
 */
public class PlaidAPIError {
    private int code;
    private String message;
    private String resolve;
    private String accessToken;

    public PlaidAPIError() {}

    public int code() {
        return code;
    }

    public String message() {
        return message;
    }

    public String resolveMesage() {
        return resolve;
    }

    public String accessToken() {
        return accessToken;
    }
}
