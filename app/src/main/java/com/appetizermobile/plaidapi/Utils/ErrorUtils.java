package com.appetizermobile.plaidapi.Utils;

import com.appetizermobile.plaidapi.Constant;
import com.appetizermobile.plaidapi.PlaidAPIError;
import com.appetizermobile.plaidapi.retrofit.ServiceGenerator;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 * Created by garrettfitzgerald on 8/10/16.
 */
public class ErrorUtils {

    public static PlaidAPIError parseError(retrofit2.Response<?> response) {
        Converter<ResponseBody, PlaidAPIError> converter =
                ServiceGenerator.retrofit().responseBodyConverter(PlaidAPIError.class, new Annotation[0]);

        PlaidAPIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new PlaidAPIError();
        }

        return error;
    }
}
