package com.appetizermobile.plaidapi;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appetizermobile.plaidapi.Dialogs.MFAEnterCodeDialog;
import com.appetizermobile.plaidapi.Dialogs.MFAQuestionDialog;
import com.appetizermobile.plaidapi.Dialogs.MFASendCodeDialog;
import com.appetizermobile.plaidapi.Utils.ErrorUtils;
import com.appetizermobile.plaidapi.components.ViewAnimation.ViewAnimation;
import com.appetizermobile.plaidapi.retrofit.ServiceGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements
        MFASendCodeDialog.MFACodeDialogListener, MFAEnterCodeDialog.MFAEnterCodeListener,
        MFAQuestionDialog.MFAQuestionDialogListener {
    private static final String TAG = "MainActivity";

    private EditText mUsernameView;
    private EditText mPasswordView;
    private Button mLogin;
    private Spinner mBanks;
    private TextView mUserPrompt;

    private String mAccessToken;

    private static final PlaidApi.PlaidApiService SERVICE =
            ServiceGenerator.createService(PlaidApi.PlaidApiService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUsernameView = (EditText) findViewById(R.id.username);
        mPasswordView = (EditText) findViewById(R.id.password);
        mUserPrompt = (TextView) findViewById(R.id.user_prompt_text_view);

        mBanks = (Spinner) findViewById(R.id.bank_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.bank_array, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBanks.setAdapter(adapter);

        mLogin = (Button) findViewById(R.id.login);
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Login button pressed");

                String username = mUsernameView.getText().toString();
                String password = mPasswordView.getText().toString();

                String type = Constant.getTypeFromBankName(mBanks.getSelectedItem().toString());
                String bankName = mBanks.getSelectedItem().toString();

                addConnectUser(username, password, type, bankName);

            }
        });

    }

    private void addConnectUser(String username, String password, final String bankCode, final String bankName) {
        Log.d(TAG, "addConnectUser");

        Call<PlaidApi.PlaidResponse> addConnectUser = SERVICE.connect(
                Constant.clientId,
                Constant.secret,
                username,
                password,
                bankCode,
                "{\"login_only\":\"true\",\"webhook\":\"http://requestb.in/19u8lh21\",\"list\":\"true\"}"
        );

        addConnectUser.enqueue(new Callback<PlaidApi.PlaidResponse>() {
            @Override
            public void onResponse(Call<PlaidApi.PlaidResponse> call,
                                   Response<PlaidApi.PlaidResponse> response) {
                int responseCode = response.code();
                Log.d(TAG, "Response status code for addConnectUser: " + responseCode);
                Log.d(TAG, "Response is successful= " + Boolean.toString(response.isSuccessful()));

                if (response.isSuccessful()) {

                    PlaidApi.PlaidResponse decodedResponse = response.body();

                    mAccessToken = decodedResponse.access_token;

                    if (responseCode == 200) { //Success
                        Log.d(TAG, "Accounts=" + decodedResponse.accounts);
                        Log.d(TAG, "Transactions= " + decodedResponse.transactions);
                        Log.d(TAG, "Access token= " + mAccessToken);

                        startActivity(TransactionActivity.newIntent(MainActivity.this, decodedResponse.access_token,bankName));


                    } else if (responseCode == 201) { //MFA required
                        String type = decodedResponse.type;
                        Log.d(TAG, "Type= " + type);
                        Log.d(TAG, "List options= " + decodedResponse.mfa);

                        mAccessToken = decodedResponse.access_token;


                        if (type.equals("questions")) {
                            List<Map<String, String>> questions
                                    = (List<Map<String, String>>) decodedResponse.mfa;

                            String firstQuestion = questions.get(0).get("question");

                            MFAQuestionDialog mfaFragment
                                    = MFAQuestionDialog.newInstance(firstQuestion, "");
                            mfaFragment.show(getSupportFragmentManager(), "MFA");
                            Log.d(TAG, "Questions= " + questions);

                        } else if (type.equals("list")) {
                            ArrayList<Map<String, String>> sendOptions
                                    = (ArrayList<Map<String, String>>) decodedResponse.mfa;

                            String[] sendMethods = new String[sendOptions.size()];

                            int i = 0;
                            for (Map<String, String> sendMethod : sendOptions) {
                                String sendType = sendMethod.get("type");
                                String mask = sendMethod.get("mask");

                                sendType = sendType.substring(0, 1).toUpperCase()
                                        + sendType.substring(1);

                                String userPrompt = sendType + ": " + mask;

                                sendMethods[i] = userPrompt;

                                i++;
                            }

                            Log.d(TAG, "Send methods= " + Arrays.toString(sendMethods));

                            DialogFragment mfaCodeDialog =
                                    MFASendCodeDialog.newInstance(sendMethods);
                            mfaCodeDialog.show(getSupportFragmentManager(), "Code");
                            Log.d(TAG, "Send Options= " + sendOptions);
                        }
                    }

                } else {
                    PlaidAPIError error = ErrorUtils.parseError(response);
                    mUserPrompt.setText(error.resolveMesage());
                    ViewAnimation.shake(mUserPrompt);
                }

            }

            @Override
            public void onFailure(Call<PlaidApi.PlaidResponse> call, Throwable t) {
                Log.e(TAG, "onFailure", t);
            }
        });
    }

    @Override
    public void onSelectSendMethod(String mask) {
        Log.d(TAG, "The mask you chose: " + mask);

        Call<PlaidApi.PlaidResponse> sendCode = SERVICE.sendCode(
                Constant.clientId,
                Constant.secret,
                mAccessToken,
                "{\"send_method\":{\"mask\":\"" + mask + "\"}}"

        );

        sendCode.enqueue(new Callback<PlaidApi.PlaidResponse>() {
            @Override
            public void onResponse(Call<PlaidApi.PlaidResponse> call,
                                   Response<PlaidApi.PlaidResponse> response) {
                int responseCode = response.code();
                Log.d(TAG, "Response status code for sendCode: " + responseCode);
                Log.d(TAG, "Response is successful= " + Boolean.toString(response.isSuccessful()));

                if (response.isSuccessful()) {
                    PlaidApi.PlaidResponse decodedResponse = response.body();

                    if (responseCode == 201) {
                        Map<String, String> messageMap = (Map<String, String>) decodedResponse.mfa;
                        String message = messageMap.get("message");

                        Log.d(TAG, "Message= " + message);
                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                        MFAEnterCodeDialog enterCodeDialog = MFAEnterCodeDialog.newInstance("");
                        enterCodeDialog.show(getSupportFragmentManager(), "Enter Code");

                    }
                } else {
                    PlaidAPIError error = ErrorUtils.parseError(response);
                    Log.d(TAG, "Error message= " + error.resolveMesage());
                    Log.d(TAG, "Send code was not successful");
                }

            }

            @Override
            public void onFailure(Call<PlaidApi.PlaidResponse> call, Throwable t) {
                Log.e(TAG, "onFailure", t);
            }
        });

    }

    @Override
    public void onCodeEntered(final String code) {

        Call<PlaidApi.PlaidResponse> authorizeMFA = SERVICE.mfaAuthorization(
                Constant.clientId,
                Constant.secret,
                code,
                mAccessToken
        );

        authorizeMFA.enqueue(new Callback<PlaidApi.PlaidResponse>() {
            @Override
            public void onResponse(Call<PlaidApi.PlaidResponse> call,
                                   Response<PlaidApi.PlaidResponse> response) {
                int responseCode = response.code();
                Log.d(TAG, "Response code for authorizeMFA= " + responseCode);

                if (response.isSuccessful()) {

                    Log.d(TAG, "Response was successful");

                    if (responseCode == 200) {

                       Call<PlaidApi.PlaidResponse> upgrade = SERVICE.upgrade(
                               Constant.clientId,
                               Constant.secret,
                               mAccessToken,
                               "connect",
                               "{\"webhook\":\"http://requestb.in/19u8lh21\"}"

                       );

                        upgrade.enqueue(new Callback<PlaidApi.PlaidResponse>() {
                            @Override
                            public void onResponse(Call<PlaidApi.PlaidResponse> call, Response<PlaidApi.PlaidResponse> response) {
                                int responseCode = response.code();
                                Log.d(TAG, "Response code for upgrade= " + responseCode);

                                if (response.isSuccessful()) {
                                    Log.d(TAG, "Response for upgrade successful");
                                    startActivity(TransactionActivity.newIntent(MainActivity.this,
                                            mAccessToken, mBanks.getSelectedItem().toString()));
                                } else {
                                    Log.d(TAG, "Response for upgrade is unsuccessful");
                                }
                            }

                            @Override
                            public void onFailure(Call<PlaidApi.PlaidResponse> call, Throwable t) {
                                Log.e(TAG, "onFailure", t);
                            }
                        });

                    }

                } else {

                    if (responseCode == 402) { //Wrong answer

                        PlaidAPIError error = ErrorUtils.parseError(response);

                        MFAEnterCodeDialog mfaEnterCodeDialog =
                                MFAEnterCodeDialog.newInstance(error.resolveMesage());
                        mfaEnterCodeDialog.show(getSupportFragmentManager(), "Enter code");

                        Log.d(TAG, "Response was unsuccessful");
                        Log.d(TAG, "Error message= " + error.resolveMesage());

                    }

                }

            }

            @Override
            public void onFailure(Call<PlaidApi.PlaidResponse> call, Throwable t) {
                Log.e(TAG, "onFailure", t);
            }
        });
        Log.d(TAG, "Entered code= " + code);
    }

    @Override
    public void onSubmit(String answer, final String question) {
        Log.d(TAG, "Answer= " + answer);

        Call<PlaidApi.PlaidResponse> authorizeMFA = SERVICE.mfaAuthorization(
                Constant.clientId,
                Constant.secret,
                answer,
                mAccessToken
        );

        authorizeMFA.enqueue(new Callback<PlaidApi.PlaidResponse>() {
            @Override
            public void onResponse(Call<PlaidApi.PlaidResponse> call,
                                   Response<PlaidApi.PlaidResponse> response) {
                int responseCode = response.code();
                Log.d(TAG, "Response code for authorizeMFA(question)= " + responseCode);

                if (response.isSuccessful()) {
                    Log.d(TAG, "Response was successful");

                    PlaidApi.PlaidResponse decodedResponse = response.body();

                    if (responseCode == 201) {  //Further questions need to be answered
                        List<Map<String, String>> questions =
                                (List<Map<String, String>>) decodedResponse.mfa;

                        String mfaQuestion = questions.get(0).get("question");
                        MFAQuestionDialog mfaQuestionDialog =
                                MFAQuestionDialog.newInstance(mfaQuestion, "");
                        mfaQuestionDialog.show(getSupportFragmentManager(), "MFA Question");

                    } else if (responseCode == 200) { //Success
                        //Start account activity
                        String bankName = mBanks.getSelectedItem().toString();
                        startActivity(TransactionActivity.newIntent(MainActivity.this, mAccessToken, bankName));
                        //startActivity(AccountActivity.newIntent(MainActivity.this, mAccessToken));
                    }
                } else {

                    if (responseCode == 402) { //Wrong answer

                        PlaidAPIError error = ErrorUtils.parseError(response);

                        MFAQuestionDialog mfaQuestionDialog =
                                MFAQuestionDialog.newInstance(question, error.resolveMesage());
                        mfaQuestionDialog.show(getSupportFragmentManager(), "MFA question");

                    }
                    Log.d(TAG, "Response was unsuccessful");
                }
            }

            @Override
            public void onFailure(Call<PlaidApi.PlaidResponse> call, Throwable t) {
                Log.e(TAG, "onFailure", t);
            }
        });
    }
}
