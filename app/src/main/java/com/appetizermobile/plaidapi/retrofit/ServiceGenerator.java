package com.appetizermobile.plaidapi.retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by garrettfitzgerald on 8/17/16.
 */
public class ServiceGenerator {

    public static final String API_URL = "https://tartan.plaid.com";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static HttpLoggingInterceptor logging = new HttpLoggingInterceptor();


    private static Retrofit.Builder builder =
            new Retrofit.Builder()
            .baseUrl(API_URL)
            .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient
                .addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BASIC))
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES).build())
                .build();

        return retrofit.create(serviceClass);
    }

    public static Retrofit retrofit() {
        return builder.build();
    }
}
