package com.appetizermobile.plaidapi.components.ViewAnimation;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

/**
 * Created by garrettfitzgerald on 8/8/16.
 */
public class ViewAnimation {

    static int _repeatNumber;

    public static void shake(final View view) {

        _repeatNumber = 2;

        final int toPosition = 10;
        final int duration = 25;

        final Animation animation1 = new TranslateAnimation(0, toPosition, 0, 0);
        animation1.setDuration(duration);
        animation1.setFillAfter(true);

        animation1.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {


            }

            @Override
            public void onAnimationEnd(Animation animation) {

                Animation animation2 = new TranslateAnimation(toPosition, -toPosition, 0, 0);
                animation2.setDuration(duration);
                animation2.setFillAfter(true);

                animation2.setAnimationListener(new Animation.AnimationListener() {

                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        Animation animation3 = new TranslateAnimation(-toPosition, 0, 0, 0);
                        animation3.setDuration(duration);
                        animation3.setFillAfter(true);

                        animation3.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {

                                if (_repeatNumber > 0) {

                                    _repeatNumber--;

                                    view.startAnimation(animation1);
                                }

                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {


                            }
                        });

                        view.startAnimation(animation3);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                view.startAnimation(animation2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {


            }
        });

        view.startAnimation(animation1);
    }
}
