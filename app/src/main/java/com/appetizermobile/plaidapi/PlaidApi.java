package com.appetizermobile.plaidapi;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.PATCH;
import retrofit2.http.POST;

/**
 * Created by garrettfitzgerald on 7/28/16.
 */
public class PlaidApi {

    static class PlaidResponse {

        List<Map<String, Object>> accounts;

        List<Map<String, Object>> transactions;

        String type;

        Object mfa;

        String access_token;
    }

    public interface PlaidApiService {
        @FormUrlEncoded
        @POST("/connect")
        Call<PlaidResponse> connect(
                @Field("client_id") String clientId,
                @Field("secret") String secret,
                @Field("username") String username,
                @Field("password") String password,
                @Field("type") String type,
                @Field("options") String options

        );

        @FormUrlEncoded
        @POST("/auth/get")
        Call<PlaidResponse> getAuthData(
                @Field("client_id") String clientId,
                @Field("secret") String secret,
                @Field("access_token") String accessToken
        );

        @FormUrlEncoded
        @POST("/connect/step")
        Call<PlaidResponse> mfaAuthorization(
                @Field("client_id") String clientId,
                @Field("secret") String secret,
                @Field("mfa") String mfa,
                @Field("access_token") String accessToken
        );

        @FormUrlEncoded
        @POST("/connect/step")
        Call<PlaidResponse> sendCode(
                @Field("client_id") String clientId,
                @Field("secret") String secret,
                @Field("access_token") String accessToken,
                @Field("options") String sendMethod
        );

        @FormUrlEncoded
        @POST("/connect/get")
        Call<PlaidResponse> getTransactions(
                @Field("client_id") String clientId,
                @Field("secret") String secret,
                @Field("access_token") String accessToken,
                @Field("options") String options
        );


        @FormUrlEncoded
        @POST("/upgrade")
        Call<PlaidResponse> upgrade(
                @Field("client_id") String clientId,
                @Field("secret") String secret,
                @Field("access_token") String access_token,
                @Field("upgrade_to") String product,
                @Field("options") String options
        );
    }
}
