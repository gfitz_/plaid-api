package com.appetizermobile.plaidapi.database;

/**
 * Created by garrettfitzgerald on 8/15/16.
 */
public class TransactionDbSchema {
    public static final class TransactionTable {
        public static final String NAME = "transactions";

        public static final class Cols {
            public static final String TRANSACTION_ACCOUNT = "transaction_account";
            public static final String TRANSACTION_ID = "transaction_id";
            public static final String TRANSACTION_AMOUNT = "transaction_amount";
            public static final String TRANSACTION_NAME = "transaction_name";
            public static final String TRANSACTION_LOCATION = "transaction_location";
            public static final String TRANSACTION_DATE = "transaction_date";
            public static final String TRANSACTION_CATEGORY = "transaction_category";
            public static final String TRANSACTION_CATEGORY_ONE = "transaction_category_level_one";
            public static final String TRANSACTION_CATEGORY_TWO = "transaction_category_level_two";
            public static final String TRANSACTION_CATEGORY_THREE = "transaction_category_level_three";
            public static final String TRANSACTION_COORDINATES = "transaction_coordinates";
        }
    }
}
