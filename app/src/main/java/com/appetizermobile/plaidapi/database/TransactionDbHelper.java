package com.appetizermobile.plaidapi.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.appetizermobile.plaidapi.database.TransactionDbSchema.TransactionTable;

/**
 * Created by garrettfitzgerald on 8/15/16.
 */
public class TransactionDbHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "transactions.db";

    public TransactionDbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TransactionTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                TransactionTable.Cols.TRANSACTION_ACCOUNT + ", " +
                TransactionTable.Cols.TRANSACTION_ID + ", " +
                TransactionTable.Cols.TRANSACTION_AMOUNT + ", " +
                TransactionTable.Cols.TRANSACTION_NAME + ", " +
                TransactionTable.Cols.TRANSACTION_LOCATION + ", " +
                TransactionTable.Cols.TRANSACTION_COORDINATES + ", " +
                TransactionTable.Cols.TRANSACTION_DATE + ", " +
                TransactionTable.Cols.TRANSACTION_CATEGORY + ", " +
                TransactionTable.Cols.TRANSACTION_CATEGORY_ONE + ", " +
                TransactionTable.Cols.TRANSACTION_CATEGORY_TWO + ", " +
                TransactionTable.Cols.TRANSACTION_CATEGORY_THREE +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
