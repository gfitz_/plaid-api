package com.appetizermobile.plaidapi.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.appetizermobile.plaidapi.Models.Transaction;
import com.appetizermobile.plaidapi.database.TransactionDbSchema.TransactionTable;

/**
 * Created by garrettfitzgerald on 8/16/16.
 */
public class TransactionCursorWrapper extends CursorWrapper {
    public TransactionCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Transaction getTransaction() {
        String transactionAccount = getString(getColumnIndex(TransactionTable.Cols.TRANSACTION_ACCOUNT));
        String transactionId = getString(getColumnIndex(TransactionTable.Cols.TRANSACTION_ID));
        String transactionAmount = getString(getColumnIndex(TransactionTable.Cols.TRANSACTION_AMOUNT));
        String transactionName = getString(getColumnIndex(TransactionTable.Cols.TRANSACTION_NAME));
        String transactionLocation = getString(getColumnIndex(TransactionTable.Cols.TRANSACTION_LOCATION));
        String transactionCategory = getString(getColumnIndex(TransactionTable.Cols.TRANSACTION_CATEGORY));
        String transactionDate = getString(getColumnIndex(TransactionTable.Cols.TRANSACTION_DATE));
        String transactionCoordinates = getString(getColumnIndex(TransactionTable.Cols.TRANSACTION_COORDINATES));

        return new Transaction(transactionAccount, transactionId, Double.parseDouble(transactionAmount), transactionDate, transactionName, transactionLocation, transactionCoordinates, transactionCategory);
    }
}
