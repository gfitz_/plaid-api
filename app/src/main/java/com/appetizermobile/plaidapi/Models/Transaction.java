package com.appetizermobile.plaidapi.Models;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by garrettfitzgerald on 8/4/16.
 */
public class Transaction implements Comparable<Transaction> {
    private static final String TAG ="Transaction";
    private String mAccount;
    private String mId;
    private Double mAmount;
    private String mDate;
    private String mName;
    private String mCategories;
    private String mCategoryLevel1;
    private String mCategoryLevel2;
    private String mCategoryLevel3;
    private String mLocation;
    private String mCoordinates;

    public Transaction(String account, String id, Double amount, String date, String name, String location, String coordinates, String categories) {
        mAccount = account;
        mId = id;
        mAmount = amount;
        mDate = date;
        mName = name;
        mLocation = location;
        mCategories = categories;
        mCoordinates = coordinates;

        if (mCategories != null && !mCategories.equals("")) {
            String[] categoryLevels = mCategories.split(", ");
            Log.d(TAG, "Category Levels Array= " + Arrays.toString(categoryLevels));

            for (int i = 0; i < categoryLevels.length; i++) {
                if (i == 0) {
                    mCategoryLevel1 = categoryLevels[i];
                } else if (i == 1) {
                    mCategoryLevel2 = categoryLevels[i];
                } else if (i == 2) {
                    mCategoryLevel3 = categoryLevels[i];
                }
            }
        }

        Log.d(TAG, "Category level 1= " + mCategoryLevel1);
        Log.d(TAG, "Category level 2= " + mCategoryLevel2);
        Log.d(TAG, "Category level 3= " + mCategoryLevel3);
    }

    public String getAccount() {
        return mAccount;
    }

    public String getId() {
        return mId;
    }

    public Double getAmount() {
        return mAmount;
    }

    public String getName() {
        return mName;
    }

    public String getLocation() {
        return mLocation;
    }

    public String getCoordinates() {

        return mCoordinates;
    }

    public String getCategories() {
        return mCategories;
    }

    public String getCategoryLevel1() {
        return mCategoryLevel1;
    }

    public String getCategoryLevel2() {
        return mCategoryLevel2;
    }

    public String getCategoryLevel3() {
        return mCategoryLevel3;
    }

    public Date getDate() {
        String[] transactionDate = mDate.split("-");

        Date date = new Date(Integer.parseInt(transactionDate[0]),
                Integer.parseInt(transactionDate[1]),
                Integer.parseInt(transactionDate[2]));

        return date;
    }

    @Override
    public String toString() {


        return "\n\nAccount= " + mAccount +
                "\nId= " + mId +
                "\nAmount= " + mAmount +
                "\nName= " + mName +
                "\nLocation= " + mLocation +
                "\nCoordinates= " + getCoordinates() +
                "\nDate= " + mDate +
                "\nCategory= " + mCategories;
    }

    @Override
    public int compareTo(Transaction transaction) {
        Date currentDate = getDate();
        Date otherDate = transaction.getDate();
        return currentDate.compareTo(otherDate);
    }
}
