package com.appetizermobile.plaidapi.Models;

/**
 * Created by garrettfitzgerald on 8/4/16.
 */
public class Account {
    private String mId;
    private String mName;
    private String mAccountNum;
    private String mRoutingNum;
    private Double mAvailableBalance;

    public Account(String id, String name, String accountNum, String routingNum,
                   Double availableBalance) {
        mId = id;
        mName = name;
        mAccountNum = accountNum;
        mRoutingNum = routingNum;
        mAvailableBalance = availableBalance;

    }

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getAccountNum() {
        return mAccountNum;
    }

    public String getRoutingNum() {
        return mRoutingNum;
    }

    public Double getAvailableBalance() {
        return mAvailableBalance;
    }

    @Override
    public String toString() {
        return "Account ID= " + mId +
                "\n Account Name= " + mName +
                "\n Account Number= " + mAccountNum +
                "\n Routing Number= " + mRoutingNum +
                "\n Available Balance= " + mAvailableBalance;

    }
}
