package com.appetizermobile.plaidapi.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by garrettfitzgerald on 8/15/16.
 */
public class MFASendCodeDialog extends DialogFragment {
    private static final String TAG = "MFASendCodeDialog";

    private static final String SEND_OPTIONS_KEY = "send options";

    public interface MFACodeDialogListener {
        public void onSelectSendMethod(String mask);
    }

    MFACodeDialogListener mListener;

    public static MFASendCodeDialog newInstance(String[] sendOptions) {
        Bundle args = new Bundle();
        args.putStringArray(SEND_OPTIONS_KEY, sendOptions);

        MFASendCodeDialog mfaCodeDialog = new MFASendCodeDialog();
        mfaCodeDialog.setArguments(args);

        return mfaCodeDialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (MFACodeDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement MFACodeListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final String[] sendOptions = getArguments().getStringArray(SEND_OPTIONS_KEY);
        builder.setTitle("Send Security Code")
                .setItems(sendOptions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String[] selectionArray = sendOptions[i].split(" ");
                        String mask = selectionArray[1];

                        mListener.onSelectSendMethod(mask);
                    }
                });

        return builder.create();
    }
}
