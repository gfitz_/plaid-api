package com.appetizermobile.plaidapi.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.appetizermobile.plaidapi.R;
import com.appetizermobile.plaidapi.components.ViewAnimation.ViewAnimation;

/**
 * Created by garrettfitzgerald on 8/15/16.
 */
public class MFAEnterCodeDialog extends DialogFragment {
    private static final String TAG ="MFAEnterCodeDialog";

    private static final String ERROR_MESSAGE_KEY = "error message";

    public interface MFAEnterCodeListener {
        public void onCodeEntered(String code);
    }

    MFAEnterCodeListener mListener;

    public static MFAEnterCodeDialog newInstance(String errorMessage) {
        Bundle args = new Bundle();
        args.putString(ERROR_MESSAGE_KEY, errorMessage);

        MFAEnterCodeDialog dialog = new MFAEnterCodeDialog();
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (MFAEnterCodeListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement MFAEnterCodeListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_mfa_enter_code, null);

        final EditText code = (EditText) view.findViewById(R.id.code_enter_edit_text);

        TextView error = (TextView) view.findViewById(R.id.error_message_text_view);
        String errorMessage = getArguments().getString(ERROR_MESSAGE_KEY);

        error.setText(errorMessage);

        if (!errorMessage.equals(""))
            ViewAnimation.shake(error);

        builder.setView(view)
                .setMessage("Enter Code:")
                .setPositiveButton("Enter", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String enteredCode = code.getText().toString();
                        mListener.onCodeEntered(enteredCode);
                        Log.d(TAG, "Enter button pressed");
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d(TAG, "Cancel button pressed");
                    }
                });
        return builder.create();
    }
}
