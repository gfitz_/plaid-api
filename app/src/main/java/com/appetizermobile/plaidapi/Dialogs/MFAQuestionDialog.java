package com.appetizermobile.plaidapi.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.appetizermobile.plaidapi.R;
import com.appetizermobile.plaidapi.components.ViewAnimation.ViewAnimation;

/**
 * Created by garrettfitzgerald on 8/9/16.
 */
public class MFAQuestionDialog extends DialogFragment {
    private static final String TAG = "MFAQuestionDialog";
    private static final  String QUESTION_KEY = "question";
    private static final String USER_MESSAGE_KEY = "user message";

    private TextView mQuestionView;
    private EditText mAnswerView;
    private TextView mUserMessage;
    private String mQuestion;

    public interface MFAQuestionDialogListener {
        public void onSubmit(String answer, String question);
    }

    private MFAQuestionDialogListener mListener;

    public static MFAQuestionDialog newInstance(String question, String errorMessage) {
        MFAQuestionDialog mfaDialog = new MFAQuestionDialog();

        Bundle args = new Bundle();
        args.putString(QUESTION_KEY, question);
        args.putString(USER_MESSAGE_KEY, errorMessage);

        mfaDialog.setArguments(args);

        return mfaDialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (MFAQuestionDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement MFAQuestionDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_mfa_questions, null);
        mQuestionView = (TextView) view.findViewById(R.id.mfa_question_text_view);
        mQuestionView.setText(mQuestion);

        mAnswerView = (EditText) view.findViewById(R.id.mfa_answer_text_view);

        mUserMessage = (TextView) view.findViewById(R.id.user_message);

        String userMessage = getArguments().getString(USER_MESSAGE_KEY);
        mUserMessage.setText(userMessage);

        if (!userMessage.equals(""))
            ViewAnimation.shake(mUserMessage);

        builder.setView(view);
        builder.setMessage("MFA Required");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d(TAG, "Submit Button Clicked");
                String answer = mAnswerView.getText().toString();
                mListener.onSubmit(answer, mQuestion);
            }
        });

        return builder.create();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQuestion = getArguments().getString(QUESTION_KEY);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(TAG, "onCancel");
    }
}
