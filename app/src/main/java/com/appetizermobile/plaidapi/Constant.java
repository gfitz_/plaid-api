package com.appetizermobile.plaidapi;

import android.content.res.Resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by garrettfitzgerald on 8/4/16.
 */
public abstract class Constant {

    public static final String clientId = "5798ec8e66710877408d0716";
    public static final String secret = "cae083d6bebb4eaf305cb5f1ff1a04";

    //Institution names
    public static final String TD = "TD";
    public static final String AMERICAN_EXPRESS = "American Express";
    public static final String CHASE = "Chase";
    public static final String WELLS_FARGO = "Wells Fargo";
    public static final String BANK_OF_AMERICA = "Bank of America";
    public static final String CITI = "Citi";
    public static final String NAVY_FEDERAL_CREDIT_UNION = "Navy Federal Credit Union";
    public static final String SUNTRUST = "SunTrust";
    public static final String US_BANK = "US Bank";

    //Institution codes
    private static final String TD_CODE = "td";
    private static final String WELLS_FARGO_CODE = "wells";
    private static final String BANK_OF_AMERICA_CODE = "bofa";
    private static final String CITI_CODE = "citi";
    private static final String NAVY_FEDERAL_CREDIT_UNION_CODE = "nfcu";
    private static final String SUNTRUST_CODE = "suntrust";
    private static final String US_BANK_CODE = "us";
    private static final String AMERICAN_EXPRESS_CODE = "amex";
    private static final String CHASE_CODE = "chase";

    //Bank code map
    public static final HashMap<String, String> BANK_CODES = loadBankCodes();


    private static HashMap<String, String> loadBankCodes() {
        HashMap<String, String> bankCodes = new HashMap<>();

        bankCodes.put(TD, TD_CODE);
        bankCodes.put(WELLS_FARGO, WELLS_FARGO_CODE);
        bankCodes.put(BANK_OF_AMERICA, BANK_OF_AMERICA_CODE);
        bankCodes.put(CITI, CITI_CODE);
        bankCodes.put(NAVY_FEDERAL_CREDIT_UNION, NAVY_FEDERAL_CREDIT_UNION_CODE);
        bankCodes.put(SUNTRUST, SUNTRUST_CODE);
        bankCodes.put(US_BANK, US_BANK_CODE);
        bankCodes.put(AMERICAN_EXPRESS, AMERICAN_EXPRESS_CODE);
        bankCodes.put(CHASE, CHASE_CODE);

        return bankCodes;
    }

    public static String getMonth(int month){
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return monthNames[month];
    }

    public static String getTypeFromBankName(String bankName) {
            return BANK_CODES.get(bankName);
    }
}
